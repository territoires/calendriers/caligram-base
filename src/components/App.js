import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Polyglot from 'node-polyglot';
import { connect } from 'react-redux';
import { Header, Footer } from 'components';

import { routes } from '../routes';

@connect(
  state => ({
    locale: state.ui.locale,
    location: state.router.location,
    translations: state.ui.translations,
  }),
)
export default class App extends Component {
  static propTypes = {
    children: PropTypes.any,
    locale: PropTypes.string.isRequired,
    location: PropTypes.object.isRequired,
    setPageTitle: PropTypes.func.isRequired,
    translations: PropTypes.array.isRequired,
  };

  render() {
    const {
      children,
      locale,
      location,
      setPageTitle,
      translations,
    } = this.props; // eslint-disable-line no-shadow

    const phrases = require(`./../locales/${locale}/base.js`);

    const polyglot = new Polyglot({
      locale,
      phrases,
    });

    const isWidget = location.pathname.slice(0, 7) === '/widget';
    const routeIdToFind = isWidget ? 'widget' : 'index';
    const homeRoute = routes.find((r) => r.id === routeIdToFind && r.locale === locale);
    const homePath = homeRoute ? homeRoute.path : '/';

    return (
      <div className="App">
        {!isWidget && (<Header homePath={homePath} locale={locale} polyglot={polyglot}
          query={location.query}
          translations={translations.filter((t) => t.locale !== locale)} />)}
        {React.cloneElement(children, { setPageTitle })}
        {!isWidget && <Footer />}
      </div>
    );
  }
}
