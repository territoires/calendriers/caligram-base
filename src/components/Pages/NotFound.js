import React from 'react';
import { Page } from 'caligram-react';

export default function NotFound() {
  return (
    <Page title="Oups!">
      <p>Cette page n’existe pas.</p>
    </Page>
  );
}
