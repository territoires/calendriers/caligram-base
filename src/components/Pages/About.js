import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Polyglot from 'node-polyglot';
import { Page } from 'caligram-react';
import { connect } from 'react-redux';
import DocumentMeta from 'react-document-meta';
import { setLocale, setTranslations } from 'redux/modules/ui';
import connectData from 'helpers/connectData';
import { routes } from '../../routes';

import config from '../../config';

function fetchData(getState, dispatch, location) {
  const promises = [];

  const localRoutes = routes.filter((r) => r.id === 'about');
  promises.push(dispatch(setTranslations(localRoutes)));

  const currentRoute = localRoutes.find((r) => r.path === location.pathname);
  promises.push(dispatch(setLocale(currentRoute.locale)));

  return Promise.all(promises);
}

@connectData(fetchData)
@connect(
  state => ({
    locale: state.ui.locale,
  })
)
export default class About extends Component {
  static propTypes = {
    locale: PropTypes.string.isRequired,
  };

  render() {
    const { locale } = this.props; // eslint-disable-line no-shadow

    const phrases = require(`./../../locales/${locale}/pages/about.js`);

    const polyglot = new Polyglot({
      locale,
      phrases,
    });

    return (
      <div>
      <DocumentMeta {...config.app} />
      <Page title={polyglot.t('title')}>
        <p>{polyglot.t('content')}</p>
      </Page>
      </div>
    );
  }
}
