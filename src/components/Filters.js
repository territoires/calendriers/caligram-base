import React from 'react';
import { HeadingBlock, SelectionList } from 'caligram-react';
import config from '../config';
import { CalendarContainer, RecurrencesButtonContainer, SearchBarContainer } from 'containers';
import { capitalize } from 'helpers/utils';

export default function Filters(props) {
  const { action, clear, polyglot } = props;

  return (
    <nav id="filters" className="Filters">
      <SearchBarContainer />
      <CalendarContainer />
      { config.app.show.recurrencesButton &&
        <HeadingBlock title={polyglot.t('recurringEvents')}>
          <RecurrencesButtonContainer />
        </HeadingBlock>
      }
      { config.app.filtersEntitiesOrder.map(entity => {
        if (config.app.entities[entity].inFilters) {
          return (
            <HeadingBlock key={`filters${capitalize(entity)}`} className={entity} title={polyglot.t(`entities.${entity}`)}>
              <SelectionList
                action={action}
                available={props[entity]}
                clear={clear.bind(this, [entity])}
                clearText={polyglot.t(`entitiesAll.${entity}`)}
                groupKey="none"
                labelKey="name"
                selected={props[`selected${capitalize(entity)}`]} />
            </HeadingBlock>
          );
        }
      }) }
    </nav>
  );
}
