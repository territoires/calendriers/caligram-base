import React from 'react';
import PropTypes from 'prop-types';
import DocumentMeta from 'react-document-meta';
import { EventDetail, EventMeta } from 'caligram-react';
import config from '../config.js';

export default function EventPage({ event, eventsPath, homePath, locale, location, push }) {
  const actions = {
    backButtonAction: () => push({ pathname: homePath }),
    audienceAction: audience => push({ pathname: eventsPath, query: { audiences: audience.id } }),
    typeAction: type => push({ pathname: eventsPath, query: { types: type.id } }),
    tagAction: tag => push({ pathname: eventsPath, query: { tags: tag.id } }),
    groupAction: group => push({ pathname: eventsPath, query: { organizations: group.id } }),
  };

  return (
    <div className="EventPage">
      <script type="application/ld+json" dangerouslySetInnerHTML={{__html: JSON.stringify(event.jsonld)}}/>
      <EventMeta component={DocumentMeta} event={event} siteName={config.app.title} twitterUsername={config.app.meta.property['twitter:creator']} />
      <EventDetail event={event} locale={locale} mapApiKey={config.app.googleMaps.apiKey} selectedDate={location.query.date} {...actions} />
    </div>
  );
}

EventPage.propTypes = {
  event: PropTypes.object.isRequired,
  location: PropTypes.any,
  params: PropTypes.object.isRequired,
};
