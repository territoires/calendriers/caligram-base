import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { Button, Checkbox, Page } from 'caligram-react';
import { routes } from '../routes';
import { app } from '../config';

export default function Entities(props) {
  const {
    disclosed,
    entities,
    entitiesTree,
    entity,
    handleToggleDisclosure,
    handleToggleShowAll,
    handleToggleTreeView,
    locale,
    polyglot,
    push,
    showAll,
    treeView,
  } = props;

  const backButtonAction = function() {
    push({ pathname: '/' });
  };

  const buildEntityLink = function(entityName, en) {
    const eventsRoute = routes.find((r) => r.id === 'events' && r.locale === locale);

    return (
      <span className="entity">
        <Link to={{ pathname: eventsRoute.path, query: { page: 1, [entityName]: en.id } }}>
          {en.name}
        </Link>
        <span className="references">{en._references !== undefined ? en._references : ''}</span>
      </span>
    );
  };

  const Item = function({ item }) {
    const liProps = { key: item.id };
    const hasChildren = item.children && item.children.length > 0;
    let isDisclosed = true;

    if (treeView && hasChildren) {
      if (app.show.disclosableEntities) {
        isDisclosed = disclosed.indexOf(item.id) > -1;
        liProps.className = `parent ${app.show.disclosableEntities && isDisclosed ? 'expanded' : 'collapsed'}`;
      } else {
        liProps.className = 'parent';
      }
    }

    const title = isDisclosed
      ? polyglot.t('collapseEntity')
      : polyglot.t('expandEntity');
    const icon = isDisclosed ? 'chevronUp' : 'chevronDown';

    return (
      <li {...liProps}>
        {buildEntityLink(entity, item)}
        {app.show.disclosableEntities && hasChildren && treeView &&
            <Button title={title} action={() => handleToggleDisclosure(item.id)} hideTitle iconLeft={icon} />
            }

            {(treeView && hasChildren && isDisclosed) ? (
              <ul className="children">
              {item.children.map((cEn) => {
                return (<Item key={`${cEn.id}-${cEn.parent_id}`} item={cEn} />);
              })}
              </ul>
              ) : ''}
      </li>
    );
  };

  return (
    <Page locale={locale} title={polyglot.t(`entitiesAll.${entity}`)} backButtonAction={backButtonAction} layout="wide" className="Entities">
      {app.show.entityFilters &&
        <ul className="filters">
          <li><Checkbox title={polyglot.t('showAll', { entity })} action={handleToggleShowAll} checked={showAll} id="toggleShowAll" /></li>
          <li><Checkbox title={polyglot.t('treeView')} action={handleToggleTreeView} checked={treeView} id="toggleTreeView" /></li>
        </ul>
      }
      <ul className={`entities ${treeView ? 'tree' : 'flat'}`}>
        {(treeView ? entitiesTree : entities).map((en) => {
          return (<Item key={`${en.id}-${en.parent_id}`} item={en} />);
        })}
      </ul>
    </Page>
  );
}

Entities.propTypes = {
  disclosed: PropTypes.array,
  entities: PropTypes.array,
  entitiesTree: PropTypes.array,
  entity: PropTypes.string,
  handleToggleDisclosure: PropTypes.func,
  handleToggleShowAll: PropTypes.func,
  handleToggleTreeView: PropTypes.func,
  locale: PropTypes.string.isRequired,
  polyglot: PropTypes.object.isRequired,
  push: PropTypes.func,
  showAll: PropTypes.bool,
  treeView: PropTypes.bool,
};
