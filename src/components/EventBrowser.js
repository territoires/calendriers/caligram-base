import React from 'react';
import PropTypes from 'prop-types';
import config from '../config';
import { Button } from 'caligram-react';
import { EventListContainer, EventsMapContainer, FiltersContainer, SearchBarContainer, ViewToggleContainer } from '../containers';

export default function EventBrowser({ mapView, push, title }) {
  return (
    <main className={`EventBrowser ${mapView ? 'map' : 'list'}`}>
      { config.app.show.mapToggle &&
        <div className="viewToggle">
          <ViewToggleContainer push={push} />
        </div>
      }
      { mapView
        ? <EventsMapContainer />
        : ( /* The extra div is a necessity of React 15. This should be removed (and styles updated) when we migrate to 16 */
          <div>
            <SearchBarContainer />
            <Button title="Filtrer par date, lieu, type d’activité..." className="filtersLink" href="#filters" width="full" />
            <div className="events">
              <EventListContainer title={title} />
            </div>
            <div className="filters">
              <FiltersContainer />
            </div>
          </div>
        )
      }
    </main>
  );
}

EventBrowser.propTypes = {
  mapView: PropTypes.bool,
  title: PropTypes.string,
};
