import React from 'react';
import config from '../config';
import { IndexLink } from 'react-router';
import { Menu } from '../components';

export default function Header({ homePath, locale, polyglot, query, setLocale, translations }) {
  return (
    <header className="Header">
      <div className="container">
        <IndexLink to={homePath} className="logo"><img src="/logo-caligram.svg" alt={config.app.title}/></IndexLink>
        <Menu locale={locale} polyglot={polyglot} query={query}
          setLocale={setLocale} translations={translations} />
      </div>
    </header>
  );
}
