import React from 'react';
import config from '../config';
import { EventBrowserContainer, ShowcaseContainer } from 'containers';

export default function Home({ polyglot }) {
  return (
    <div className="Home">
      { !!config.app.showcases && config.app.showcases.featured &&
        <aside>
          <ShowcaseContainer
            query={config.app.showcases.featured}
            title={polyglot.t('highlights')} />
        </aside>
      }
      <EventBrowserContainer />
    </div>
  );
}
