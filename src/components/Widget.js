import React from 'react';
import { EventList } from 'caligram-react';
import { EventListContainer } from '../containers';

export default function Widget() {
  return (
    <EventListContainer>
      {(props) => {
        return <EventList {...props} filters={[]} totalPages={1} target="_blank"/>;
      }}
    </EventListContainer>
  );
}
