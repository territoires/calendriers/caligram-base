import React from 'react';
import { DynamicMapContainer } from '../containers';

export default function EventsMap() {
  return (
    <div className="EventsMap">
      <div className="map">
        <DynamicMapContainer />
      </div>
    </div>
  );
}
