import React from 'react';
import { IndexLink, Link } from 'react-router';
import { getRouteHelper } from 'helpers/utils';
import { stringify } from 'querystring';
import { routes } from '../routes';

export default function Menu({ locale, polyglot, query, translations }) {
  const getRoute = getRouteHelper(routes, locale);
  const queryString = stringify({
    ...query,
    page: 1,
  });

  return (
    <nav className="Menu">
      <ul>
        <li key="index"><IndexLink to={getRoute('index')} activeClassName="current">{polyglot.t('menu.home')}</IndexLink></li>
        <li key="about"><Link to={getRoute('about')} activeClassName="current">{polyglot.t('menu.about')}</Link></li>
        <li key="types"><Link to={getRoute('types')} activeClassName="current">{polyglot.t('menu.byType')}</Link></li>
        <li key="themes"><Link to={getRoute('themes')} activeClassName="current">{polyglot.t('menu.byTheme')}</Link></li>
        <li key="venues"><Link to={getRoute('venues')} activeClassName="current">{polyglot.t('menu.byVenue')}</Link></li>
        <li key="groups"><Link to={getRoute('groups')} activeClassName="current">{polyglot.t('menu.byOrganization')}</Link></li>

        <li key="separator-1" className="separator"></li>

        {translations.map((tr) => {
          return (<li key={`${tr.id}_${tr.locale}`}>
            <Link to={`${tr.path}?${queryString}`}>
              {polyglot.t(`locales.short.${tr.locale}`)}
            </Link>
          </li>);
        })}
      </ul>
    </nav>
  );
}
