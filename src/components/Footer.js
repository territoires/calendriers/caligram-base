import React from 'react';

export default function Footer() {
  return (
    <footer className="Footer">
      <p className="poweredBy">Propulsé par <a href="http://pro.caligram.com" target="_blank">Caligram Pro</a></p>
    </footer>
  );
}
