/* eslint no-console: 0 */
import tokens from './../../tokens';
import caligram from './../../caligram';

export default function serverTokenMiddleware() {
  return function(req, res, next) {
    if (req.path === '/error') return next();

    caligram.serverAuthenticate((err, serverToken) => {
      if (err) {
        const message = JSON.parse(err.data).message;
        console.log(message);
        if (message === 'Client authentication failed.') {
          tokens.delAllTokens();
        }
        res.send(message);
      } else {
        req.serverToken = serverToken;
        next();
      }
    });
  };
}
