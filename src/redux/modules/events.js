import { app } from 'config';
import { stringify } from 'querystring';

const GEO_LOAD = 'events/GEO_LOAD';
const GEO_LOAD_FAIL = 'events/GEO_LOAD_FAIL';
const GEO_LOAD_SUCCESS = 'events/GEO_LOAD_SUCCESS';
const LOAD = 'events/LOAD';
const LOAD_FAIL = 'events/LOAD_FAIL';
const LOAD_SUCCESS = 'events/LOAD_SUCCESS';
const SET_MAP_CENTER = 'events/SET_MAP_CENTER';
const SET_MAP_VIEW = 'events/SET_MAP_VIEW';
const SET_RECURRENCES = 'events/SET_RECURRENCES';
const SET_SELECTED_RANGE = 'events/SET_SELECTED_RANGE';

const initialState = {
  data: [],
  geoData: [],
  lastQuery: '',
  loaded: false,
  loading: false,
  map: {
    centerLat: app.map.centerLat,
    centerLng: app.map.centerLng,
    initialZoom: app.map.initialZoom,
  },
  mapView: false,
  pagination: {
    current_page: null,
    total_pages: null,
  },
  promise: null,
  recurrences: true,
  selectedRange: {
    start: null,
    end: null,
  },
};

export default function events(state = initialState, action = {}) {
  switch (action.type) {
    case GEO_LOAD:
      if (state.promise && state.promise.cancel) {
        state.promise.cancel();
      }
      return {
        ...state,
        loaded: false,
        loading: true,
        promise: action.promise,
      };
    case GEO_LOAD_FAIL:
      return {
        ...state,
        error: action.error,
        loaded: false,
        promise: null,
      };
    case GEO_LOAD_SUCCESS:
      return {
        ...state,
        loaded: true,
        loading: false,
        geoData: action.result.data ? action.result.data : action.result,
        promise: null,
      };
    case LOAD:
      if (state.promise && state.promise.cancel) {
        state.promise.cancel();
      }
      return {
        ...state,
        loaded: false,
        loading: true,
        promise: action.promise,
      };
    case LOAD_FAIL:
      return {
        ...state,
        error: action.error,
        loaded: false,
        promise: null,
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        lastQuery: action.query,
        loaded: true,
        loading: false,
        data: action.result.data ? action.result.data : action.result,
        pagination: action.result.meta.pagination ? action.result.meta.pagination : null,
        promise: null,
      };
    case SET_MAP_CENTER:
      const map = state.map;
      const nc = action.mapData;

      map.centerLat = nc.center.lat || map.centerLat;
      map.centerLng = nc.center.lng || map.centerLng;
      map.initialZoom = nc.zoom || map.initialZoom;

      return {
        ...state,
        map,
      };
    case SET_MAP_VIEW:
      return {
        ...state,
        mapView: action.mapView,
      };
    case SET_RECURRENCES:
      return {
        ...state,
        recurrences: action.recurrences,
      };
    case SET_SELECTED_RANGE:
      return {
        ...state,
        loading: false,
        selectedRange: {
          ...action.range,
        },
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.events && globalState.events.loaded;
}

export function isNewQuery(globalState, query) {
  return globalState.events.lastQuery !== `?${stringify(query)}`;
}

export function index(query) {
  const queryString = typeof query === 'string'
    ? query
    : '?' + stringify(query);
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => {
      return client.get(
        (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
          `events${queryString}`
      );
    },
    query: queryString,
  };
}

export function geoIndex(center, radius) {
  const queryRadius = Math.max(
    100,
    Math.floor(parseInt(radius, 10))
  );

  const query = {
    distance: `${center.lat},${center.lng},${queryRadius}`,
    per: 1000,
    recurrences: false,
  };

  return {
    types: [GEO_LOAD, GEO_LOAD_SUCCESS, GEO_LOAD_FAIL],
    promise: (client) => {
      return client.get(
        (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
          `events?${stringify(query)}`
      );
    },
  };
}

export function selectRange(range) {
  return {
    type: SET_SELECTED_RANGE,
    range,
  };
}

export function setMapCenter(mapData) {
  return {
    mapData,
    type: SET_MAP_CENTER,
  };
}

export function setMapView(mapView) {
  return {
    type: SET_MAP_VIEW,
    mapView,
  };
}

export function setRecurrences(recurrences) {
  return {
    type: SET_RECURRENCES,
    recurrences,
  };
}
