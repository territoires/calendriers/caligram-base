import { stringify } from 'querystring';

const LOAD = 'showcase/LOAD';
const LOAD_SUCCESS = 'showcase/LOAD_SUCCESS';
const LOAD_FAIL = 'showcase/LOAD_FAIL';

export default function showcase(state = {}, action = {}) {
  switch (action.type) {
    case LOAD:
      const collection = state[action.slug];
      if (collection && collection.promise && collection.promise.cancel) {
        collection.promise.cancel();
      }
      return {
        ...state,
        [action.slug]: {
          ...state[action.slug],
          [action.locale]: {
            loaded: false,
            loading: true,
            promise: action.promise,
          },
        },
      };
    case LOAD_FAIL:
      return {
        ...state,
        [action.slug]: {
          ...state[action.slug],
          [action.locale]: {
            error: action.error,
            loaded: false,
            promise: null,
          },
        },
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        [action.slug]: {
          ...state[action.slug],
          [action.locale]: {
            data: action.result.data ? action.result.data : action.result,
            loaded: true,
            loading: false,
            pagination: action.result.meta.pagination
              ? action.result.meta.pagination
              : null,
            promise: null,
          },
        },
      };
    default:
      return state;
  }
}

export function index(query, locale) {
  const defaultOptions = {
    order: 'start_date',
    per: 10,
    recurrences: false,
    lang: locale,
  };
  const options = { ...defaultOptions, ...query };

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    locale,
    slug: stringify(query),
    promise: (client) => {
      return client.get(
        (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
          `events?${stringify(options)}`
      );
    },
  };
}

export function isLoaded(globalState, query, locale) {
  const slug = stringify(query);
  return !!(globalState.showcase
    && globalState.showcase[slug]
    && globalState.showcase[slug][locale]
    && globalState.showcase[slug][locale].loaded);
}
