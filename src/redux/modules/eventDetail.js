const LOAD = 'eventDetail/LOAD';
const LOAD_SUCCESS = 'eventDetail/LOAD_SUCCESS';
const LOAD_FAIL = 'eventDetail/LOAD_FAIL';

const initialState = {
  error: null,
  data: null,
  loaded: false,
  loading: false,
};

export default function event(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loaded: false,
        loading: true,
        error: null,
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loaded: true,
        loading: false,
        data: action.result.data ? action.result.data : action.result,
        error: null,
      };
    case LOAD_FAIL:
      return {
        ...state,
        loaded: false,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export function isLoaded(globalState, locale) {
  return globalState.event
    && globalState.event.loaded
    && globalState.event.data.lang === locale;
}

export function show(id, locale) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(`events/${id}?lang=${locale}&include=jsonld`),
  };
}
