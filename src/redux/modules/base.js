import {
  filterCollectionFromParams,
  findInCollectionOrAdd,
  mergeCollections,
  orderByField,
  splitIdsList,
} from 'helpers/utils';
import Polyglot from 'node-polyglot';
import { stringify } from 'querystring';
import { app } from 'config';

export default function BaseReducer(entity) {
  const ADD = entity + '/ADD';
  const LOAD = entity + '/LOAD';
  const LOAD_FAIL = entity + '/LOAD_FAIL';
  const LOAD_SUCCESS = entity + '/LOAD_SUCCESS';
  const LOAD_TOPS = entity + '/LOAD_TOPS';
  const LOAD_TOPS_FAIL = entity + '/LOAD_TOPS_FAIL';
  const LOAD_TOPS_SUCCESS = entity + '/LOAD_TOPS_SUCCESS';
  const SET_SELECTED = entity + '/SET_SELECTED';
  const SET_MENU = entity + '/SET_MENU';
  const TOGGLE_DISCLOSED = entity + '/TOGGLE_DISCLOSED';
  const SET_SHOW_ALL = entity + '/SET_SHOW_ALL';
  const SET_TREE_VIEW = entity + '/SET_TREE_VIEW';

  const config = app.entities[entity];
  const initialData = (config.initialData || {});
  const initialState = {
    data: initialData,
    config,
    loaded: false,
    loading: false,
    menu: [],
    selected: [],
    disclosed: [],
    showAll: config.showAll !== undefined ? config.showAll : false,
    treeView: config.treeView !== undefined ? config.treeView : false,
    topsLoaded: {},
    topsLoading: {},
    tops: {},
  };

  function toggleItem(sourceList, item) {
    const index = sourceList.indexOf(item);
    switch (index > -1) {
      case true: // Present
        sourceList.splice(index, 1);
        break;
      case false: // Absent
      default:
        sourceList.push(item);
    }
    return sourceList;
  }

  function mergeEntities(toMerge, target, locale = 'fr') {
    const phrases = require(`./../../locales/${locale}/base.js`);

    const polyglot = new Polyglot({
      locale,
      phrases,
    });

    const parsedData = toMerge.map((ent) => {
      return {
        group: entity,
        id: ent.id,
        label: ent.name,
        name: ent.name,
        parent_id: ent.parent_id,
        references: ent.references,
        tag: polyglot.t(`filters.${entity}`),
      };
    });

    return mergeCollections(parsedData, target);
  }

  return {
    reducer(state = initialState, action = {}) {
      switch (action.type) {
        case ADD:
          return {
            ...state,
            data: {
              ...state.data,
              [action.locale]: findInCollectionOrAdd(action.item, state.data[action.locale] || []),
            },
          };
        case LOAD:
          return {
            ...state,
            loaded: false,
            loading: true,
          };
        case LOAD_FAIL:
          return {
            ...state,
            loaded: false,
            loading: false,
          };
        case LOAD_SUCCESS:
          let data;
          // This exists only because all LOAD, LOAD_TOPS and INDEX merge
          // results between executions, which is useful to keep an history
          // of existing values for filters, but not for lists.
          // We should eventually refactor this to always keep a merged list
          // of seen items, and a separate collection of active items.
          let newData = [];
          if (Array.isArray(action.result)) {
            action.result.forEach((aData) => {
              newData = newData.concat(aData.data);
            });
          } else {
            newData = action.result.data;
          }

          if (action.flush) {
            data = newData;
          } else {
            data = newData
              ? mergeEntities(newData, state.data[action.locale] || [], action.locale)
              : newData;
          }

          return {
            ...state,
            data: {
              ...state.data,
              [action.locale]: data,
            },
            loaded: true,
            loading: false,
          };
        case LOAD_TOPS:
          return {
            ...state,
            topsLoaded: {
              ...state.topsLoaded,
              [action.locale]: false,
            },
            topsLoading: {
              ...state.topsLoading,
              [action.locale]: true,
            },
          };
        case LOAD_TOPS_FAIL:
          return {
            ...state,
            topsLoaded: {
              ...state.topsLoaded,
              [action.locale]: false,
            },
            topsLoading: {
              ...state.topsLoading,
              [action.locale]: false,
            },
          };
        case LOAD_TOPS_SUCCESS:
          return {
            ...state,
            data: {
              ...state.data,
              [action.locale]: action.result.data
                ? mergeEntities(action.result.data, state.data[action.locale] || [])
                : state.data[action.locale],
            },
            topsLoaded: {
              ...state.topsLoaded,
              [action.locale]: true,
            },
            topsLoading: {
              ...state.topsLoading,
              [action.locale]: false,
            },
            tops: {
              ...state.tops,
              [action.locale]: action.result.data
                ? mergeEntities(action.result.data, state.tops[action.locale] || [])
                : state.tops[action.locale],
            },
          };
        case TOGGLE_DISCLOSED:
          return {
            ...state,
            disclosed: toggleItem(state.disclosed, action.value),
          };
        case SET_MENU:
          const filteredCollection = filterCollectionFromParams(state.data[action.locale], action.menu);

          return {
            ...state,
            menu: config.menuOrder ? orderByField(
              filteredCollection,
              config.menuOrder
            ) : filteredCollection,
          };
        case SET_SELECTED:
          return {
            ...state,
            selected: filterCollectionFromParams(state.data[action.locale], action.selected, entity),
          };
        case SET_SHOW_ALL:
          return {
            ...state,
            showAll: action.value,
          };
        case SET_TREE_VIEW:
          return {
            ...state,
            treeView: action.value,
          };
        default:
          return state;
      }
    },

    add(object) {
      return {
        type: ADD,
        item: object,
      };
    },

    areTopsLoaded(globalState, locale) {
      return globalState[entity] && globalState[entity].topsLoaded[locale];
    },

    index(showAll, treeView, query = {}, locale, flush = false) {
      const querystring = stringify(
        Object.assign({}, {
          direction: (showAll ? 'asc' : 'desc'),
          order: (showAll ? 'name' : 'references'),
          per: 1000,
          lang: locale,
        }, query)
      );

      let promise;
      if (!showAll) {
        if (treeView) {
          promise = (client) => {
            const fetchParents = (res) => {
              const process = new Promise(function(resolve) {
                const fetchedIds = [];
                const parentIds = [];

                res.data.forEach((eId) => {
                  fetchedIds.push(eId.id);
                  if (eId.parent_id) {
                    parentIds.push(eId.parent_id);
                  }
                });

                const unfetchedIds = parentIds.filter((pId) => {
                  return fetchedIds.indexOf(pId) === -1;
                });

                if (unfetchedIds.length > 0) {
                  client.get(`/${entity}?lang=${locale}&id=${unfetchedIds}&lang=${locale}`)
                    .then((results) => {
                      fetchParents({ data: results.data.concat(res.data) }).then(resolve);
                    });
                } else {
                  resolve(res);
                }
              });

              return process;
            };

            return client.get(
              (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
                `${entity}?${querystring}`
            ).then(fetchParents);
          };
        } else {
          promise = (client) => {
            return client.get(
              (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
                `${entity}?${querystring}`
            );
          };
        }
      } else {
        promise = (client) => {
          return client.get(`/${entity}?${querystring}`);
        };
      }

      return {
        types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
        flush,
        promise,
        locale,
      };
    },

    isLoaded(globalState) {
      return globalState[entity] && globalState[entity].loaded;
    },

    isMenuProvided() {
      return Array.isArray(config.menu) && config.menu.length > 0;
    },

    isMenuRequired() {
      return config.inFilters;
    },

    load(ids, locale) {
      if (!Array.isArray(ids)) {
        return { type: LOAD_FAIL };
      }

      return {
        locale,
        types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
        promise: (client) => {
          return client.get(
            (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
              `${entity}?id=${ids.join(',')}`
          );
        },
      };
    },

    loadTops(locale) {
      return {
        types: [LOAD_TOPS, LOAD_TOPS_SUCCESS, LOAD_TOPS_FAIL],
        locale,
        promise: (client) => {
          return client.get(
            (process.env.CALENDAR_ID ? `calendars/${process.env.CALENDAR_ID}/` : '') +
              `${entity}?lang=${locale}&order=references&direction=desc&per=${config.limit || 10}`
          );
        },
      };
    },

    toggleDisclosed(value) {
      return {
        type: TOGGLE_DISCLOSED,
        value,
      };
    },

    setMenu(menu, locale) {
      return {
        locale,
        menu,
        type: SET_MENU,
      };
    },

    setSelected(selected, locale) {
      return {
        locale,
        type: SET_SELECTED,
        selected: Array.isArray(selected) ? selected : splitIdsList(selected),
      };
    },

    setShowAll(value) {
      return {
        type: SET_SHOW_ALL,
        value,
      };
    },

    setTreeView(value) {
      return {
        type: SET_TREE_VIEW,
        value,
      };
    },
  };
}
