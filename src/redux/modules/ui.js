const CHANGE_LOCALE = 'ui/CHANGE_LOCALE';
const SET_TRANSLATIONS = 'ui/SET_TRANSLATIONS';

const initialState = {
  locale: 'fr',
  translations: [],
};

export default function ui(state = initialState, action = {}) {
  switch (action.type) {
    case CHANGE_LOCALE:
      return {
        ...state,
        locale: action.locale,
      };
    case SET_TRANSLATIONS:
      return {
        ...state,
        translations: action.translations,
      };
    default:
      return state;
  }
}

export function setLocale(locale) {
  return {
    type: CHANGE_LOCALE,
    locale,
  };
}

export function setTranslations(translations) {
  return {
    type: SET_TRANSLATIONS,
    translations,
  };
}
