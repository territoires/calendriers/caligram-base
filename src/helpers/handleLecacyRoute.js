import superagent from 'superagent';

export default async function handleLegacyRoute(route, client) {
  const endpoint = 'http://services.murmitoyen.com/helpers/get_ids/';

  try {
    const { body } = await superagent.get(endpoint + route);
    const externalId = body.data.external_id;
    const seriesId = body.data.mm_series_id;

    if (externalId) {
      const externalIdQuery = `/events?range=all&source=murmitoyen&external_id=${externalId}`;
      const externalIdResult = await client.get(externalIdQuery);

      if (externalIdResult.data.length) {
        return externalIdResult.data[0].url;
      } else if (seriesId) {
        const seriesIdQuery = `/events?range=all&source=murmitoyen&mm_series_id=${seriesId}`;
        const seriesIdResult = await client.get(seriesIdQuery);

        if (seriesIdResult.data.length) {
          return seriesIdResult.data[0].url;
        }
      } else {
        console.error('services.murmitoyen.com did not return a series_id');
        return false;
      }
    } else {
      console.error('services.murmitoyen.com did not return an external_id');
      return false;
    }
  } catch (err) {
    console.error(err);
    return false;
  }
}
