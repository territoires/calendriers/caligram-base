import React from 'react';
import { IndexRoute, Route } from 'react-router';
import { About, NotFound } from 'components';
import config from './config';
import {
  AppContainer,
  EventPageContainer,
  EventsContainer,
  HomeContainer,
  OrganizationsContainer,
  TagsContainer,
  TypesContainer,
  VenuesContainer,
  WidgetContainer,
} from 'containers';

const { fr: baseFr, en: baseEn } = config.app.i18n.basePaths;

export const routes = [
  {
    id: 'index',
    locale: 'fr',
    path: baseFr || '/',
    component: HomeContainer,
  },
  {
    id: 'index',
    locale: 'en',
    path: baseEn || '/en',
    component: HomeContainer,
  },
  {
    id: 'events',
    locale: 'fr',
    path: baseFr + '/evenements',
    component: EventsContainer,
  },
  {
    id: 'events',
    locale: 'en',
    path: baseEn + '/events',
    component: EventsContainer,
  },
  {
    id: 'event',
    locale: 'fr',
    path: baseFr + '/evenements/:slug/:id',
    match: /\/evenements\//,
    component: EventPageContainer,
  },
  {
    id: 'event',
    locale: 'en',
    path: baseEn + '/events/:slug/:id',
    match: /\/events\//,
    component: EventPageContainer,
  },
  {
    id: 'widget',
    locale: 'fr',
    path: baseFr + '/widget',
    component: WidgetContainer,
  },
  {
    id: 'widget',
    locale: 'en',
    path: baseEn + '/widget',
    component: WidgetContainer,
  },
  {
    id: 'about',
    locale: 'fr',
    path: baseFr + '/a-propos',
    component: About,
  },
  {
    id: 'about',
    locale: 'en',
    path: baseEn + '/about',
    component: About,
  },
  {
    id: 'types',
    locale: 'fr',
    path: baseFr + '/types',
    component: TypesContainer,
  },
  {
    id: 'types',
    locale: 'en',
    path: baseEn + '/types',
    component: TypesContainer,
  },
  {
    id: 'themes',
    locale: 'fr',
    path: baseFr + '/themes',
    component: TagsContainer,
  },
  {
    id: 'themes',
    locale: 'en',
    path: baseEn + '/themes',
    component: TagsContainer,
  },
  {
    id: 'venues',
    locale: 'fr',
    path: baseFr + '/lieux',
    component: VenuesContainer,
  },
  {
    id: 'venues',
    locale: 'en',
    path: baseEn + '/venues',
    component: VenuesContainer,
  },
  {
    id: 'groups',
    locale: 'fr',
    path: baseFr + '/groupes',
    component: OrganizationsContainer,
  },
  {
    id: 'groups',
    locale: 'en',
    path: baseEn + '/groups',
    component: OrganizationsContainer,
  },
];

export default () => {
  return (
    <Route path="/" component={AppContainer}>
      {routes.map((route) => {
        if (route.path === '/') {
          return <IndexRoute key={`${route.id}_${route.locale}`} component={route.component} />;
        }

        return <Route key={`${route.id}_${route.locale}`} component={route.component} path={route.path} />;
      })}

      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};
