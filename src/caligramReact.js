import { CaligramReact } from 'caligram-react';
import { Link } from 'react-router';

CaligramReact.config({
  // This configuration object will be passed to caligram-react.
  // See https://gitlab.com/territoires/caligram-react/blob/master/src/config.js
  linkComponent: Link,
});
