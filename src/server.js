/* eslint no-console: 0 */
import dotenv from 'dotenv';
import Express from 'express';
import React from 'react';
import ReactDOM from 'react-dom/server';
import favicon from 'serve-favicon';
import compression from 'compression';
import httpProxy from 'http-proxy';
import path from 'path';
import config from './config';
import createStore from './redux/create';
import ApiClient from './helpers/ApiClient';
import Html from './helpers/Html';
import PrettyError from 'pretty-error';
import http from 'http';

import {ReduxRouter} from 'redux-router';
import createHistory from 'history/lib/createMemoryHistory';
import {reduxReactRouter, match} from 'redux-router/server';
import {Provider} from 'react-redux';
import qs from 'query-string';
import getRoutes from './routes';
import getStatusFromRoutes from './helpers/getStatusFromRoutes';
import handleLegacyRoute from './helpers/handleLecacyRoute';
import './caligramReact';

import serverTokenMiddleware from './redux/middleware/serverTokenMiddleware';
import tokens from './tokens';

import cookieParser from 'cookie-parser';
import acceptLanguage from 'accept-language';

acceptLanguage.languages(['fr', 'en']);

dotenv.load();

const pretty = new PrettyError();
const app = new Express();
const server = new http.Server(app);
const proxy = httpProxy.createProxyServer({
  target: process.env.CALIGRAM_API_URL,
  changeOrigin: true,
});

// Set the API required headers on every request.
proxy.on('proxyReq', function(proxyReq) {
  proxyReq.setHeader('Accept', 'application/vnd.caligram.v1+json');
  proxyReq.removeHeader('Pragma');
  proxyReq.setHeader('Cache-control', 'public, max-age=300');

  const clientToken = tokens.getLastToken();
  if (clientToken) {
    clientToken.expired_at = new Date(clientToken.expired_at);

    if (clientToken && clientToken.access_token) {
      proxyReq.setHeader('Authorization', clientToken.token_type + ' ' + clientToken.access_token);
    }
  }
});

// Send to proxy response cookie to the client.
proxy.on('proxyRes', function(proxyRes, req, res) {
  if (proxyRes.headers['set-cookie']) {
    res.append('set-cookie', proxyRes.headers['set-cookie']);
  }
  delete proxyRes.headers['Pragma'];
  proxyRes.headers['Cache-control'] = 'public, max-age=300';
});

app.use(cookieParser());
app.use(serverTokenMiddleware());
app.use(compression());
app.use(favicon(path.join(__dirname, '..', 'static', 'favicon.ico')));

function detectLocale(req) {
  const cookieLocale = req.cookies.locale;

  return acceptLanguage.get(cookieLocale || req.headers['accept-language']) || 'fr';
}

app.use(Express.static(path.join(__dirname, '..', 'static')));

// Proxy to API server
app.use('/api', (req, res) => {
  proxy.web(req, res);
});

// added the error handling to avoid https://github.com/nodejitsu/node-http-proxy/issues/527
proxy.on('error', (error, req, res) => {
  let json;
  if (error.code !== 'ECONNRESET') {
    console.log('proxy error', error);
  }
  if (!res.headersSent) {
    res.writeHead(500, {'content-type': 'application/json'});
  }

  json = {error: 'proxy_error', reason: error.message};
  res.end(JSON.stringify(json));
});

app.use((req, res) => {
  if (__DEVELOPMENT__) {
    // Do not cache webpack stats: the script file would change since
    // hot module replacement is enabled in the development env
    webpackIsomorphicTools.refresh();
  }
  const client = new ApiClient(req);
  const locale = detectLocale(req);
  const store = createStore(reduxReactRouter, getRoutes, createHistory, client);

  function hydrateOnClient() {
    res.send('<!doctype html>\n' +
      ReactDOM.renderToString(<Html assets={webpackIsomorphicTools.assets()} store={store}/>));
  }

  if (__DISABLE_SSR__) {
    hydrateOnClient();
    return;
  }

  store.dispatch(match(req.originalUrl, (error, redirectLocation, routerState) => {
    if (redirectLocation) {
      res.redirect(redirectLocation.pathname + redirectLocation.search);
    } else if (error) {
      console.error('ROUTER ERROR:', pretty.render(error));
      res.status(500);
      hydrateOnClient();
    } else if (!routerState) {
      res.status(500);
      hydrateOnClient();
    } else {
      // Workaround redux-router query string issue:
      // https://github.com/rackt/redux-router/issues/106
      if (routerState.location.search && !routerState.location.query) {
        routerState.location.query = qs.parse(routerState.location.search);
      }

      store.getState().router.then(async () => {
        const component = (
          <Provider store={store} key="provider">
            <ReduxRouter/>
          </Provider>
        );
        const status = getStatusFromRoutes(routerState.routes);
        if (status) {
          if (status === 404) {
            const requestedRoute = routerState.params.splat;
            const result = await handleLegacyRoute(requestedRoute, client);
            if (result) {
              res.status(301);
              res.redirect(result);
              return;
            }
          }
          res.status(status);
        }
        res.cookie('locale', locale, { maxAge: (new Date() * 0.001) + (365 * 24 * 3600) });
        res.send('<!doctype html>\n' +
          ReactDOM.renderToString(<Html assets={webpackIsomorphicTools.assets()}
                                        component={component}
                                        store={store}/>));
      }).catch((err) => {
        console.error('DATA FETCHING ERROR:', pretty.render(err));
        res.status(500);
        hydrateOnClient();
      });
    }
  }));
});

if (config.port) {
  server.listen(config.port, (err) => {
    if (err) {
      console.error(err);
    }
    console.info('----\n==> ✅  %s is running.', config.app.title);
    console.info('==> 💻  Open http://localhost:%s in a browser to view the app.', config.port);
  });
} else {
  console.error('==>     ERROR: No PORT environment variable has been specified');
}
