import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BodyClassName from 'react-body-classname';
import Polyglot from 'node-polyglot';
import { connect } from 'react-redux';
import DocumentMeta from 'react-document-meta';
import { index as eventIndex, isNewQuery, setMapView, setRecurrences } from 'redux/modules/events';
import { setLocale, setTranslations } from 'redux/modules/ui';
import connectData from 'helpers/connectData';
import { Home } from 'components';

import { routes } from '../routes';
import config from '../config';

function fetchData(getState, dispatch, location) {
  const promises = [];
  const state = getState();

  const localRoutes = routes.filter((r) => r.id === 'index');
  promises.push(dispatch(setTranslations(localRoutes)));

  const currentRoute = localRoutes.find((r) => r.path === location.pathname);
  promises.push(dispatch(setLocale(currentRoute.locale)));

  const query = {
    lang: currentRoute.locale,
    ...location.query,
  };
  if (query.recurrences) {
    promises.push(dispatch(setRecurrences(query.recurrences === 'true')));
  } else {
    query.recurrences = state.events.recurrences;
  }

  if (query.mapView) {
    promises.push(dispatch(setMapView(query.mapView === 'true')));
  } else {
    query.mapView = state.events.mapView;
  }

  query.order = 'start_date';

  // force reload events with back and next browser buttons
  if (query.mapView === 'false' || !query.mapView) {
    if (isNewQuery(getState(), query)) {
      promises.push(dispatch(eventIndex(query)));
    }
  }

  return Promise.all(promises);
}

@connectData(fetchData)
@connect(state => ({
  locale: state.ui.locale,
}), {})
export default class HomeContainer extends Component {
  static propTypes = {
    locale: PropTypes.string.isRequired,
    setPageTitle: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.props.setPageTitle(config.app.title);
  }

  render() {
    const { locale } = this.props;
    const phrases = require(`./../locales/${locale}/base.js`);

    const polyglot = new Polyglot({
      locale,
      phrases,
    });

    return (
      <BodyClassName className="home">
        <div>
          <DocumentMeta {...config.app} />
          <Home polyglot={polyglot} />
        </div>
      </BodyClassName>
    );
  }
}
