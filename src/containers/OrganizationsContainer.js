import React, { Component } from 'react';
import BodyClassName from 'react-body-classname';
import { index } from 'redux/modules/organizations';
import { setLocale, setTranslations } from 'redux/modules/ui';
import { EntitiesContainer } from '../containers';
import connectData from 'helpers/connectData';

import { routes } from '../routes';

function fetchData(getState, dispatch, location) {
  const promises = [];

  const localRoutes = routes.filter((r) => r.id === 'groups');
  promises.push(dispatch(setTranslations(localRoutes)));

  const currentRoute = localRoutes.find((r) => r.path === location.pathname);
  promises.push(dispatch(setLocale(currentRoute.locale)));

  const state = getState().organizations;
  promises.push(dispatch(index(state.showAll, state.treeView, {}, currentRoute.locale)));

  return Promise.all(promises);
}

@connectData(fetchData)
export default class OrganizationsContainer extends Component {
  render() {
    return (
      <BodyClassName className="organizations">
        <EntitiesContainer entity="organizations" />
      </BodyClassName>
    );
  }
}
