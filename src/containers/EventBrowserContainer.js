import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import Polyglot from 'node-polyglot';
import { EventBrowser } from 'components';

@connect(state => ({
  locale: state.ui.locale,
  mapView: state.events.mapView,
}), { push })
export default class EventBrowserContainer extends Component {
  static propTypes = {
    locale: PropTypes.string.isRequired,
    mapView: PropTypes.bool,
    title: PropTypes.string,
    push: PropTypes.func.isRequired,
  };

  render() {
    const { locale } = this.props;
    const phrases = require(`./../locales/${locale}/base.js`);

    const polyglot = new Polyglot({
      locale,
      phrases,
    });

    return <EventBrowser title={polyglot.t('allEvents')} {...this.props} />;
  }
}
