import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Polyglot from 'node-polyglot';
import { connect } from 'react-redux';
import { Filters } from '../components';
import { push } from 'redux-router';
import { updateSelections } from 'helpers/utils';
import { setSelected as setSelectedAudiences } from 'redux/modules/audiences';
import { setSelected as setSelectedOrganizations } from 'redux/modules/organizations';
import { setSelected as setSelectedRegions } from 'redux/modules/regions';
import { setSelected as setSelectedTags } from 'redux/modules/tags';
import { setSelected as setSelectedTypes } from 'redux/modules/types';
import { setSelected as setSelectedVenues } from 'redux/modules/venues';
import { routes } from 'routes';

@connect(
  state => ({
    audiences: state.audiences.menu,
    location: state.router.location,
    locale: state.ui.locale,
    organizations: state.organizations.menu,
    regions: state.regions.menu,
    selectedAudiences: state.audiences.selected,
    selectedOrganizations: state.organizations.selected,
    selectedRange: state.events.selectedRange,
    selectedRegions: state.regions.selected,
    selectedTags: state.tags.selected,
    selectedTypes: state.types.selected,
    selectedVenues: state.venues.selected,
    tags: state.tags.menu,
    types: state.types.menu,
    venues: state.venues.menu,
  }),
  {
    push,
    setSelectedAudiences,
    setSelectedOrganizations,
    setSelectedRegions,
    setSelectedTags,
    setSelectedTypes,
    setSelectedVenues,
  }
)
export default class FiltersContainer extends Component {
  static propTypes = {
    audiences: PropTypes.array.isRequired,
    locale: PropTypes.string.isRequired,
    location: PropTypes.object,
    organizations: PropTypes.array.isRequired,
    push: PropTypes.func,
    queryParams: PropTypes.object,
    regions: PropTypes.array.isRequired,
    selectedOrganizations: PropTypes.array.isRequired,
    selectedAudiences: PropTypes.array.isRequired,
    selectedRange: PropTypes.object.isRequired,
    selectedRegions: PropTypes.array.isRequired,
    selectedTags: PropTypes.array.isRequired,
    selectedTypes: PropTypes.array.isRequired,
    selectedVenues: PropTypes.array.isRequired,
    setSelectedAudiences: PropTypes.func.isRequired,
    setSelectedOrganizations: PropTypes.func.isRequired,
    setSelectedRegions: PropTypes.func.isRequired,
    setSelectedTags: PropTypes.func.isRequired,
    setSelectedTypes: PropTypes.func.isRequired,
    setSelectedVenues: PropTypes.func.isRequired,
    tags: PropTypes.array.isRequired,
    types: PropTypes.array.isRequired,
    venues: PropTypes.array.isRequired,
  };

  filtersAction(item) {
    const { locale, push } = this.props; // eslint-disable-line no-shadow
    const selected = {
      audiences: this.props.selectedAudiences,
      organizations: this.props.selectedOrganizations,
      regions: this.props.selectedRegions,
      types: this.props.selectedTypes,
      tags: this.props.selectedTags,
      venues: this.props.selectedVenues,
    };
    const select = {
      audiences: this.props.setSelectedAudiences,
      organizations: this.props.setSelectedOrganizations,
      regions: this.props.setSelectedRegions,
      types: this.props.setSelectedTypes,
      tags: this.props.setSelectedTags,
      venues: this.props.setSelectedVenues,
    };

    const newQuery = updateSelections(item, selected, select, locale);

    const { selectedRange: { start, end } } = this.props;
    if (start || end) {
      newQuery.range = `${start || ''},${end || ''}`;
    }

    const eventsRoute = routes.find((r) => r.id === 'events' && r.locale === locale);
    const eventsPath = eventsRoute ? eventsRoute.path : null;

    push({ pathname: eventsPath, query: newQuery });
  }

  filtersClear(types) {
    const { locale, location, push } = this.props; // eslint-disable-line no-shadow
    const queryParams = { ...location.query };

    const select = {
      audiences: this.props.setSelectedAudiences,
      organizations: this.props.setSelectedOrganizations,
      regions: this.props.setSelectedRegions,
      types: this.props.setSelectedTypes,
      tags: this.props.setSelectedTags,
      venues: this.props.setSelectedVenues,
    };

    for (const type of types) {
      select[type]([]);
      delete queryParams[type];
    }

    const eventsRoute = routes.find((r) => r.id === 'events' && r.locale === locale);
    const eventsPath = eventsRoute ? eventsRoute.path : null;

    push({ pathname: eventsPath, query: queryParams });
  }

  render() {
    const { locale } = this.props;
    const phrases = require(`./../locales/${locale}/base.js`);

    const polyglot = new Polyglot({
      locale,
      phrases,
    });

    return (<Filters action={::this.filtersAction}
      clear={::this.filtersClear}
      polyglot={polyglot}
      {...this.props} />);
  }
}
