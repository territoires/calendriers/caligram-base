import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Polyglot from 'node-polyglot';
import { SearchBar } from 'caligram-react';
import { connect } from 'react-redux';
import { autocomplete, clear } from 'redux/modules/search';
import { add as addAudience } from 'redux/modules/audiences';
import { add as addOrganization } from 'redux/modules/organizations';
import { add as addRegion } from 'redux/modules/regions';
import { add as addTag } from 'redux/modules/tags';
import { add as addType } from 'redux/modules/types';
import { add as addVenue } from 'redux/modules/venues';
import { push } from 'redux-router';
import { routes } from 'routes';

@connect(state => ({
  locale: state.ui.locale,
  location: state.router.location,
  items: state.search.items,
}), dispatch => ({
  dispatchAddAudience: function(audience) {
    return dispatch(addAudience(audience));
  },
  dispatchAddRegion: function(audience) {
    return dispatch(addRegion(audience));
  },
  dispatchAddTag: function(tag) {
    return dispatch(addTag(tag));
  },
  dispatchAddType: function(type) {
    return dispatch(addType(type));
  },
  dispatchAddOrganization: function(org) {
    return dispatch(addOrganization(org));
  },
  dispatchAddVenue: function(venue) {
    return dispatch(addVenue(venue));
  },
  dispatchAutocomplete: function(query, searchIn, extra) {
    return dispatch(autocomplete(query, searchIn, extra));
  },
  dispatchClearSearch: function() {
    return dispatch(clear());
  },
  dispatchPush: function(obj) {
    return dispatch(push(obj));
  },
}))
export default class SearchBarContainer extends Component {
  static propTypes = {
    dispatchAddAudience: PropTypes.func.isRequired,
    dispatchAddOrganization: PropTypes.func.isRequired,
    dispatchAddTag: PropTypes.func.isRequired,
    dispatchAddType: PropTypes.func.isRequired,
    dispatchAddVenue: PropTypes.func.isRequired,
    dispatchAutocomplete: PropTypes.func.isRequired,
    dispatchClearSearch: PropTypes.func.isRequired,
    dispatchPush: PropTypes.func.isRequired,
    items: PropTypes.array,
    locale: PropTypes.string.isRequired,
    location: PropTypes.object,
  };

  static contextTypes = {
    history: PropTypes.object,
    location: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: props.location.query.q || '',
    };
  }

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue,
    });
  };

  onSubmit = (event) => {
    const { dispatchPush, locale } = this.props; // eslint-disable-line no-shadow
    event.preventDefault();

    const eventsRoute = routes.find((r) => r.id === 'events' && r.locale === locale);
    const eventsPath = eventsRoute ? eventsRoute.path : null;

    dispatchPush({ pathname: eventsPath, query: { q: this.state.value } });
  }

  onSuggestionsFetchRequested = ({ value }) => {
    const query = value.trim();

    const { dispatchAutocomplete, locale } = this.props; // eslint-disable-line no-shadow

    if (this.state.timeout) {
      clearTimeout(this.state.timeout);
    }

    if (query.length >= 1) {
      this.setState({
        timeout: setTimeout(() => {
          dispatchAutocomplete(query, null, { lang: locale });
        }, 250),
      }, );
    }
  };

  onSuggestionsClearRequested = () => {
    this.props.dispatchClearSearch();
  };

  onSuggestionSelected = (polyglot) => {
    return (event, { suggestion }) => {
      event.preventDefault();
      const {
        dispatchAddAudience,
        dispatchAddOrganization,
        dispatchAddTag,
        dispatchAddType,
        dispatchAddVenue,
        dispatchPush,
      } = this.props; // eslint-disable-line no-shadow

      const query = {};
      query[suggestion.group] = suggestion.id;

      switch (suggestion.group) {
        case 'audiences':
          suggestion.tag = polyglot.t('filters.audience');
          dispatchAddAudience(suggestion);
          break;
        case 'organizations':
          suggestion.tag = polyglot.t('filters.organization');
          dispatchAddOrganization(suggestion);
          break;
        case 'tags':
          suggestion.tag = polyglot.t('filters.tag');
          dispatchAddTag(suggestion);
          break;
        case 'types':
          suggestion.tag = polyglot.t('filters.type');
          dispatchAddType(suggestion);
          break;
        case 'venues':
          suggestion.tag = polyglot.t('filters.venue');
          dispatchAddVenue(suggestion);
          break;
        case 'events':
          suggestion.tag = polyglot.t('filters.event');
          dispatchPush({ pathname: suggestion.path });
          break;
        default:
          break;
      }

      if (suggestion.group !== 'events') {
        const eventsRoute = routes.find((r) => {
          return r.id === 'events' && r.locale === this.props.locale;
        });
        const eventsPath = eventsRoute ? eventsRoute.path : null;

        dispatchPush({ pathname: eventsPath, query: query });
      }

      this.setState({
        value: '',
      });
    };
  };

  renderSectionTitle = (polyglot) => {
    return (section) => {
      return polyglot.t(`entities.${section.title}`);
    };
  };

  renderSuggestion = (polyglot) => {
    return (suggestion) => {
      return (
        <div data-group={polyglot.t(`entities.${suggestion.group}`)}>
          {suggestion.image && <img src={suggestion.image} alt={suggestion.label}/>}
          <span>{suggestion.label}</span>
          {suggestion.venue && <div className="venue">{suggestion.venue}</div>}
        </div>
      );
    };
  };

  render() {
    const { locale } = this.props;
    const phrases = require(`./../locales/${locale}/base.js`);

    const polyglot = new Polyglot({
      locale,
      phrases,
    });

    const properties = {
      locale,
      onChange: this.onChange,
      onClear: this.onSuggestionsClearRequested,
      onFetch: this.onSuggestionsFetchRequested,
      onSelect: this.onSuggestionSelected(polyglot),
      onSubmit: this.onSubmit,
      query: this.state.value,
      renderSectionTitle: this.renderSectionTitle(polyglot),
      renderSuggestion: this.renderSuggestion(polyglot),
      suggestions: this.props.items,
    };

    return <SearchBar {...properties} />;
  }
}
