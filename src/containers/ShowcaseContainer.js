import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { EventList } from 'caligram-react';
import { stringify } from 'querystring';

@connect(state => ({
  locale: state.ui.locale,
  showcases: state.showcase,
}), {push})
export default class ShowcaseContainer extends Component {
  static propTypes = {
    children: PropTypes.func,
    locale: PropTypes.string.isRequired,
    push: PropTypes.func.isRequired,
    query: PropTypes.object.isRequired,
    showcases: PropTypes.object,
    title: PropTypes.string.isRequired,
  };

  static defaultProps = {
    children: (props) => {
      const dateFormat = ev => moment(ev.start_date).format('Do MMMM');
      return (<EventList layout="carousel"
        groupBy="none"
        dateFormat={dateFormat}
        {...props} />);
    },
  };

  render() {
    const { locale, query, showcases, title } = this.props;

    const events = showcases[stringify(query)][locale]
      ? showcases[stringify(query)][locale].data
      : [];

    return (
      <div className="ShowcaseContainer">
        { this.props.children({
          events,
          locale,
          title,
        }) }
      </div>
    );
  }
}
