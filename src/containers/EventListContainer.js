import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Polyglot from 'node-polyglot';
import { push } from 'redux-router';
import { EventList } from 'caligram-react';
import moment from 'moment';
import { setSelected as setSelectedAudiences } from 'redux/modules/audiences';
import { setSelected as setSelectedOrganizations } from 'redux/modules/organizations';
import { setSelected as setSelectedRegions } from 'redux/modules/regions';
import { setSelected as setSelectedTags } from 'redux/modules/tags';
import { setSelected as setSelectedTypes } from 'redux/modules/types';
import { setSelected as setSelectedVenues } from 'redux/modules/venues';
import { updateSelections } from 'helpers/utils';
import { routes } from 'routes';

@connect(state => ({
  events: state.events.data,
  loading: state.events.loading,
  locale: state.ui.locale,
  location: state.router.location,
  pagination: state.events.pagination,
  selectedAudiences: state.audiences.selected,
  selectedOrganizations: state.organizations.selected,
  selectedRegions: state.regions.selected,
  selectedTags: state.tags.selected,
  selectedTypes: state.types.selected,
  selectedVenues: state.venues.selected,
}), {
  push,
  setSelectedAudiences,
  setSelectedOrganizations,
  setSelectedRegions,
  setSelectedTags,
  setSelectedTypes,
  setSelectedVenues,
})
export default class EventListContainer extends Component {
  static propTypes = {
    children: PropTypes.func,
    events: PropTypes.array,
    loading: PropTypes.bool,
    locale: PropTypes.string.isRequired,
    location: PropTypes.object,
    pagination: PropTypes.object,
    push: PropTypes.func.isRequired,
    selectedAudiences: PropTypes.array.isRequired,
    selectedOrganizations: PropTypes.array.isRequired,
    selectedRegions: PropTypes.array.isRequired,
    selectedTags: PropTypes.array.isRequired,
    selectedTypes: PropTypes.array.isRequired,
    selectedVenues: PropTypes.array.isRequired,
    setSelectedAudiences: PropTypes.func.isRequired,
    setSelectedOrganizations: PropTypes.func.isRequired,
    setSelectedRegions: PropTypes.func.isRequired,
    setSelectedTags: PropTypes.func.isRequired,
    setSelectedTypes: PropTypes.func.isRequired,
    setSelectedVenues: PropTypes.func.isRequired,
    title: PropTypes.string,
    widget: PropTypes.bool,
  };

  static contextTypes = {
    router: PropTypes.object,
  };

  static defaultProps = {
    children: (props) => { return <EventList {...props} />; },
    widget: false,
  };

  handlePageChange(page) {
    const { locale, location, push } = this.props; // eslint-disable-line no-shadow
    const query = {
      ...location.query,
      page: page,
    };

    const eventsRoute = routes.find((r) => r.id === 'events' && r.locale === locale);
    const eventsPath = eventsRoute ? eventsRoute.path : null;

    push({ pathname: eventsPath, query: query });
  }

  handleRemoveItem(item) {
    const { locale, location, push } = this.props; // eslint-disable-line no-shadow
    const selected = {
      audiences: this.props.selectedAudiences,
      organizations: this.props.selectedOrganizations,
      regions: this.props.selectedRegions,
      types: this.props.selectedTypes,
      tags: this.props.selectedTags,
      venues: this.props.selectedVenues,
    };
    const select = {
      audiences: this.props.setSelectedAudiences,
      organizations: this.props.setSelectedOrganizations,
      regions: this.props.setSelectedRegions,
      types: this.props.setSelectedTypes,
      tags: this.props.setSelectedTags,
      venues: this.props.setSelectedVenues,
    };
    let newQuery = { ...location.query };

    if (item.group === 'range') {
      delete newQuery.range;
    } else {
      newQuery = updateSelections(item, selected, select);
      newQuery.range = location.query.range;
    }

    const eventsRoute = routes.find((r) => r.id === 'events' && r.locale === locale);
    const eventsPath = eventsRoute ? eventsRoute.path : null;

    push({ pathname: eventsPath, query: newQuery });
  }

  handleClear() {
    [
      this.props.setSelectedAudiences,
      this.props.setSelectedOrganizations,
      this.props.setSelectedRegions,
      this.props.setSelectedTypes,
      this.props.setSelectedTags,
      this.props.setSelectedVenues,
    ].forEach((method) => method([]));

    const eventsRoute = routes.find((r) => r.id === 'events' && r.locale === this.props.locale);
    const eventsPath = eventsRoute ? eventsRoute.path : null;

    this.props.push({ pathname: eventsPath });
  }

  render() {
    const { events, loading, locale, location, pagination, title } = this.props;

    const phrases = require(`./../locales/${locale}/base.js`);

    const polyglot = new Polyglot({
      locale,
      phrases,
    });

    const {
      selectedAudiences,
      selectedOrganizations,
      selectedRegions,
      selectedTypes,
      selectedTags,
      selectedVenues,
      widget,
    } = this.props;

    const range = [];
    if (location.query.range) {
      const date = {
        id: location.query.range,
        label: moment(location.query.range.split(',')[0]).locale(locale).format('Do MMMM YYYY'),
        tag: 'Date',
        group: 'range',
      };
      range.push(date);
    }

    const selectedItems = range.concat(selectedAudiences)
      .concat(selectedOrganizations)
      .concat(selectedRegions)
      .concat(selectedTypes)
      .concat(selectedTags)
      .concat(selectedVenues);

    const properties = {
      clearFilters: ::this.handleClear,
      currentPage: pagination.current_page,
      emptyMessage: polyglot.t('noEvents'),
      events,
      filters: selectedItems,
      loading,
      locale,
      pageChange: ::this.handlePageChange,
      removeFilter: ::this.handleRemoveItem,
      title,
      target: widget ? '_blank' : null,
      totalPages: pagination.total_pages,
    };

    return this.props.children(properties);
  }
}
