import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tabs, Tab } from 'caligram-react';
import { connect } from 'react-redux';
import { setMapView } from 'redux/modules/events';

const mapDispatchToProps = (dispatch) => {
  return {
    dispatchSetMapView: (mapView) => {
      dispatch(setMapView(mapView));
    },
  };
};

@connect(
  state => ({
    location: state.router.location,
    mapView: state.events.mapView,
  }),
  mapDispatchToProps
)
export default class ViewToggleContainer extends Component {
  static propTypes = {
    children: PropTypes.func,
    location: PropTypes.object,
    mapView: PropTypes.bool.isRequired,
    dispatchSetMapView: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
  };

  static defaultProps = {
    children: ({ mapView, handleSetMapView }) => {
      return (
        <Tabs small selected={mapView ? 'map' : 'list'}>
          <Tab key="list" title="Liste" icon="list" action={() => handleSetMapView(false)} />
          <Tab key="map" title="Carte" icon="location" action={() => handleSetMapView(true)} />
        </Tabs>
      );
    },
  };

  handleSetMapView(mapView) {
    this.props.dispatchSetMapView(mapView);
    const { location, push } = this.props;
    const query = {
      ...location.query,
      mapView,
    };

    push({ pathname: location.pathname, query });
  }

  render() {
    const properties = {
      mapView: this.props.mapView,
      handleSetMapView: () => { this.handleSetMapView(!this.props.mapView); },
    };
    return this.props.children(properties);
  }
}
