import React, { Component } from 'react';
import BodyClassName from 'react-body-classname';
import { setLocale, setTranslations } from 'redux/modules/ui';
import { index } from 'redux/modules/tags';
import { EntitiesContainer } from '../containers';
import connectData from 'helpers/connectData';

import { routes } from '../routes';

function fetchData(getState, dispatch, location) {
  const promises = [];

  const localRoutes = routes.filter((r) => r.id === 'themes');
  promises.push(dispatch(setTranslations(localRoutes)));

  const currentRoute = localRoutes.find((r) => r.path === location.pathname);
  promises.push(dispatch(setLocale(currentRoute.locale)));

  const state = getState().types;
  promises.push(dispatch(index(state.showAll, state.treeView, {}, currentRoute.locale)));

  return Promise.all(promises);
}

@connectData(fetchData)
export default class TagsContainer extends Component {
  render() {
    return (
      <BodyClassName className="tags">
        <EntitiesContainer entity="tags" />
      </BodyClassName>
    );
  }
}
