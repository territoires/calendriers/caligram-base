import React, { Component } from 'react';
import BodyClassName from 'react-body-classname';
import { setLocale, setTranslations } from 'redux/modules/ui';
import { index } from 'redux/modules/types';
import { EntitiesContainer } from '../containers';
import connectData from 'helpers/connectData';

import { routes } from 'routes';

function fetchData(getState, dispatch, location) {
  const promises = [];

  const localRoutes = routes.filter((r) => r.id === 'types');
  promises.push(dispatch(setTranslations(localRoutes)));

  const currentRoute = localRoutes.find((r) => r.path === location.pathname);
  promises.push(dispatch(setLocale(currentRoute.locale)));

  const state = getState().types;
  promises.push(dispatch(index(state.showAll, state.treeView, {}, currentRoute.locale)));

  return Promise.all(promises);
}

@connectData(fetchData)
export default class TypesContainer extends Component {
  render() {
    return (
      <BodyClassName className="types">
        <EntitiesContainer entity="types" />
      </BodyClassName>
    );
  }
}
