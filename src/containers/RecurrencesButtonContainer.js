import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from 'caligram-react';
import { connect } from 'react-redux';
import Polyglot from 'node-polyglot';
import { setRecurrences } from 'redux/modules/events';

const mapDispatchToProps = (dispatch) => {
  return {
    dispatchSetRecurrences: (recurrences) => {
      dispatch(setRecurrences(recurrences));
    },
  };
};

@connect(
  state => ({
    loading: state.events.loading,
    locale: state.ui.locale,
    location: state.router.location,
    recurrences: state.events.recurrences,
  }),
  mapDispatchToProps
)
export default class RecurrencesButtonContainer extends Component {
  static propTypes = {
    children: PropTypes.func,
    dispatchSetRecurrences: PropTypes.func,
    loading: PropTypes.bool,
    locale: PropTypes.string.isRequired,
    location: PropTypes.object,
    recurrences: PropTypes.bool.isRequired,
  };

  static contextTypes = {
    router: PropTypes.object,
  };

  static defaultProps = {
    children: ({ recurrences, handleSetRecurrences, title }) => {
      return <Checkbox action={handleSetRecurrences} checked={!recurrences} id="recurrences" title={title} />;
    },
  };

  handleSetRecurrences(recurrences) {
    if (this.props.loading) {
      return;
    }

    this.props.dispatchSetRecurrences(recurrences);

    const { location } = this.props;
    const newQuery = {
      ...location.query,
      recurrences,
    };
    const newLocation = {
      ...location,
      query: newQuery,
    };

    this.context.router.push(newLocation);
  }

  render() {
    const { locale } = this.props;
    const phrases = require(`./../locales/${locale}/base.js`);

    const polyglot = new Polyglot({
      locale,
      phrases,
    });

    const properties = {
      recurrences: this.props.recurrences,
      handleSetRecurrences: () => { this.handleSetRecurrences(!this.props.recurrences); },
      title: polyglot.t('event.hideRepeatingEvents'),
    };
    return this.props.children(properties);
  }
}
