import React, { Component } from 'react';
import PropTypes from 'prop-types';
import BodyClassName from 'react-body-classname';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { isLoaded, show } from 'redux/modules/eventDetail';
import Polyglot from 'node-polyglot';
import { setLocale, setTranslations } from 'redux/modules/ui';
import connectData from 'helpers/connectData';

import { EventPage, NotFound } from '../components';
import { routes } from '../routes';

function fetchData(getState, dispatch, location, params) {
  const localRoutes = routes.filter((r) => r.id === 'event').map((r) => {
    return {
      ...r,
      base: r.path.substring(0, r.path.indexOf('/', 1)),
    };
  });
  const currentRoute = localRoutes.find((r) => location.pathname.indexOf(r.base) === 0);

  return new Promise((resolve, reject) => {
    const promises = [];
    const state = getState();

    if (!isLoaded(state, currentRoute.locale)) {
      promises.push(dispatch(show(params.id, currentRoute.locale)));
    } else {
      promises.push({ result: state.event.data });
    }

    return Promise.all(promises).then(([{ result }]) => {
      const innerPromises = [];

      innerPromises.push(dispatch(setTranslations(result.translations)));
      innerPromises.push(dispatch(setLocale(result.lang)));

      return Promise.all(innerPromises).then(resolve).catch(reject);
    });
  });
}

@connectData(fetchData)
@connect(state => ({
  event: state.eventDetail.data,
  error: state.eventDetail.error,
  locale: state.ui.locale,
  location: state.router.location,
}), { push, show })
export default class EventPageContainer extends Component {
  static propTypes = {
    params: PropTypes.object.isRequired,
    error: PropTypes.any,
    event: PropTypes.object,
    locale: PropTypes.string.isRequired,
    location: PropTypes.any,
    push: PropTypes.func.isRequired,
    setPageTitle: PropTypes.func.isRequired,
  };

  componentWillMount() {
    if (this.props.event) {
      this.props.setPageTitle(this.props.event.title);
    }
  }

  render() {
    if (this.props.error) {
      switch (this.props.error.status_code) {
        case 404:
        default:
          return <NotFound />;
      }
    }

    const { locale } = this.props;
    const phrases = require(`./../locales/${locale}/base.js`);

    const polyglot = new Polyglot({
      locale,
      phrases,
    });

    const eventsRoute = routes.find((r) => r.id === 'events' && r.locale === locale);
    const eventsPath = eventsRoute.path;

    const homeRoute = routes.find((r) => r.id === 'index' && r.locale === locale);
    const homePath = homeRoute.path;

    return (
      <BodyClassName className="event">
        <EventPage homePath={homePath} eventsPath={eventsPath} polyglot={polyglot} {...this.props} />
      </BodyClassName>
    );
  }
}
