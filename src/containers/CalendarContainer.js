import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Calendar } from 'caligram-react';
import { connect } from 'react-redux';
import { push } from 'redux-router';
import { selectRange } from 'redux/modules/events';
import { routes } from 'routes';

@connect(state => ({
  locale: state.ui.locale,
  location: state.router.location,
  selectedRangeStart: state.events.selectedRange.start,
}), {push, selectRange})
export default class CalendarContainer extends Component {
  static propTypes = {
    locale: PropTypes.string,
    location: PropTypes.object,
    push: PropTypes.func.isRequired,
    selectRange: PropTypes.func.isRequired,
    selectedRangeStart: PropTypes.string,
  };

  onSelect(date) {
    const {
      locale,
      location,
      push, // eslint-disable-line no-shadow
      selectRange, // eslint-disable-line no-shadow
      selectedRangeStart,
    } = this.props;

    const format = 'YYYY-MM-DD HH:mm:ss';

    const start = date.clone().startOf('day').format(format);
    const end = date.clone().endOf('day').format(format);

    const isSameDay = date.isSame(selectedRangeStart, 'day');
    const query = {
      ...location.query,
      page: 1,
    };

    if (isSameDay) {
      delete query.range;
      selectRange({ start: null, end: null });
    } else {
      query.range = `${start},${end}`;
      selectRange({ start, end });
    }

    const eventsRoute = routes.find((r) => r.id === 'events' && r.locale === locale);
    const eventsPath = eventsRoute ? eventsRoute.path : null;

    push({ pathname: eventsPath, query: query });

    return true;
  }

  render() {
    const { locale, selectedRangeStart } = this.props;
    let date = null;

    if (selectedRangeStart) {
      date = moment(selectedRangeStart);
    }

    return (
      <Calendar date={date} locale={locale} onSelect={::this.onSelect} />
    );
  }
}
