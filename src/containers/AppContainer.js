import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { setLocale } from 'redux/modules/ui';
import { parseRange, splitIdsList } from 'helpers/utils';
import { App } from 'components';
import connectData from 'helpers/connectData';
import {
  areTopsLoaded as areAudiencesTopsLoaded,
  isLoaded as isAudiencesLoaded,
  isMenuProvided as isAudiencesMenuProvided,
  isMenuRequired as isAudiencesMenuRequired,
  load as loadAudiences,
  loadTops as loadTopsAudiences,
  setMenu as setAudiencesMenu,
  setSelected as setSelectedAudiences,
} from 'redux/modules/audiences';
import { selectRange } from 'redux/modules/events';
import {
  areTopsLoaded as areOrganizationsTopsLoaded,
  isLoaded as isOrganizationsLoaded,
  isMenuProvided as isOrganizationsMenuProvided,
  isMenuRequired as isOrganizationsMenuRequired,
  load as loadOrganizations,
  loadTops as loadTopsOrganizations,
  setMenu as setOrganizationsMenu,
  setSelected as setSelectedOrganizations,
} from 'redux/modules/organizations';
import {
  areTopsLoaded as areRegionsTopsLoaded,
  isLoaded as isRegionsLoaded,
  isMenuProvided as isRegionsMenuProvided,
  isMenuRequired as isRegionsMenuRequired,
  load as loadRegions,
  loadTops as loadTopsRegions,
  setMenu as setRegionsMenu,
  setSelected as setSelectedRegions,
} from 'redux/modules/regions';
import {
  index as showcaseIndex,
  isLoaded as isShowcaseLoaded,
} from 'redux/modules/showcase';
import {
  areTopsLoaded as areTagsTopsLoaded,
  isLoaded as isTagsLoaded,
  isMenuProvided as isTagsMenuProvided,
  isMenuRequired as isTagsMenuRequired,
  load as loadTags,
  loadTops as loadTopsTags,
  setMenu as setTagsMenu,
  setSelected as setSelectedTags,
} from 'redux/modules/tags';
import {
  areTopsLoaded as areTypesTopsLoaded,
  isLoaded as isTypesLoaded,
  isMenuProvided as isTypesMenuProvided,
  isMenuRequired as isTypesMenuRequired,
  load as loadTypes,
  loadTops as loadTopsTypes,
  setMenu as setTypesMenu,
  setSelected as setSelectedTypes,
} from 'redux/modules/types';
import {
  areTopsLoaded as areVenuesTopsLoaded,
  isLoaded as isVenuesLoaded,
  isMenuProvided as isVenuesMenuProvided,
  isMenuRequired as isVenuesMenuRequired,
  load as loadVenues,
  loadTops as loadTopsVenues,
  setMenu as setVenuesMenu,
  setSelected as setSelectedVenues,
} from 'redux/modules/venues';
import { routes } from 'routes';

import config from '../config';
import '../stylesheets/global.scss';

function fetchData(getState, dispatch, location) {
  const entityTypes = [
    {
      areTopsLoaded: areAudiencesTopsLoaded,
      isLoaded: isAudiencesLoaded,
      isMenuProvided: isAudiencesMenuProvided,
      isMenuRequired: isAudiencesMenuRequired,
      load: loadAudiences,
      loadTops: loadTopsAudiences,
      name: 'audiences',
      setMenu: setAudiencesMenu,
      setSelected: setSelectedAudiences,
    },
    {
      areTopsLoaded: areOrganizationsTopsLoaded,
      isLoaded: isOrganizationsLoaded,
      isMenuProvided: isOrganizationsMenuProvided,
      isMenuRequired: isOrganizationsMenuRequired,
      load: loadOrganizations,
      loadTops: loadTopsOrganizations,
      name: 'organizations',
      setMenu: setOrganizationsMenu,
      setSelected: setSelectedOrganizations,
    },
    {
      areTopsLoaded: areRegionsTopsLoaded,
      isLoaded: isRegionsLoaded,
      isMenuProvided: isRegionsMenuProvided,
      isMenuRequired: isRegionsMenuRequired,
      load: loadRegions,
      loadTops: loadTopsRegions,
      name: 'regions',
      setMenu: setRegionsMenu,
      setSelected: setSelectedRegions,
    },
    {
      areTopsLoaded: areTagsTopsLoaded,
      isLoaded: isTagsLoaded,
      isMenuProvided: isTagsMenuProvided,
      isMenuRequired: isTagsMenuRequired,
      load: loadTags,
      loadTops: loadTopsTags,
      name: 'tags',
      setMenu: setTagsMenu,
      setSelected: setSelectedTags,
    },
    {
      areTopsLoaded: areTypesTopsLoaded,
      isLoaded: isTypesLoaded,
      isMenuProvided: isTypesMenuProvided,
      isMenuRequired: isTypesMenuRequired,
      load: loadTypes,
      loadTops: loadTopsTypes,
      name: 'types',
      setMenu: setTypesMenu,
      setSelected: setSelectedTypes,
    },
    {
      areTopsLoaded: areVenuesTopsLoaded,
      isLoaded: isVenuesLoaded,
      isMenuProvided: isVenuesMenuProvided,
      isMenuRequired: isVenuesMenuRequired,
      load: loadVenues,
      loadTops: loadTopsVenues,
      name: 'venues',
      setMenu: setVenuesMenu,
      setSelected: setSelectedVenues,
    },
  ];

  const topsPromises = [];

  const currentRoute = routes.find((r) => {
    return r.path === location.pathname || (r.match && location.pathname.match(r.match));
  });
  const locale = currentRoute ? currentRoute.locale : getState().ui.locale;

  topsPromises.push(dispatch(setLocale(locale)));

  const missingMenuTypes = entityTypes.filter((type) => {
    return type.isMenuRequired() && !type.isMenuProvided();
  });
  missingMenuTypes.forEach((type) => {
    if (!type.areTopsLoaded(getState(), locale)) {
      topsPromises.push(dispatch(type.loadTops(locale)));
    }
  });

  return Promise.all(topsPromises).then(() => {
    const loadPromises = [];
    entityTypes.forEach((type) => {
      const state = getState()[type.name];

      if (!type.isLoaded(getState())) {
        const ids = splitIdsList(location.query[type.name])
          .concat(getState()[type.name].config.menu || [])
          .filter((id) => {
            return !(state.data[locale], []).find(item => item.id === id);
          });

        if (ids.length > 0) {
          loadPromises.push(
            dispatch(type.load(ids))
          );
        }
      }
    });

    if (config.app.showcases) {
      for (const showcase in config.app.showcases) {
        if (config.app.showcases.hasOwnProperty(showcase)) {
          const showcaseQuery = config.app.showcases[showcase];
          if (!isShowcaseLoaded(getState(), showcaseQuery, locale)) {
            loadPromises.push(dispatch(showcaseIndex(showcaseQuery, locale)));
          }
        }
      }
    }

    return Promise.all(loadPromises).then(() => {
      const promises = [];

      if (!!location.query.range) {
        const range = parseRange(location.query.range);
        promises.push(dispatch(selectRange(range)));
      } else {
        promises.push(dispatch(selectRange({ start: null, end: null })));
      }

      entityTypes.forEach((type) => {
        const state = getState()[type.name];

        if (type.isMenuRequired()) {
          if (type.isMenuProvided()) {
            promises.push(dispatch(type.setMenu(state.config.menu, locale)));
          } else {
            promises.push(dispatch(type.setMenu(
              state.tops[locale].map((item) => { return item.id; }),
              locale
            )));
          }
        }

        promises.push(dispatch(type.setSelected(location.query[type.name], locale)));
      });

      return Promise.all(promises);
    });
  });
}

@connectData(fetchData)
export default class AppContainer extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    location: PropTypes.object,
  };

  static contextTypes = {
    store: PropTypes.object.isRequired,
  };

  state = {
    title: null,
  };

  componentDidUpdate() {
    this.logPageView();
  }

  setPageTitle(pageTitle) {
    this.setState({
      title: pageTitle,
    });
  }

  logPageView() {
    if (typeof window !== 'undefined' && !!window.ga) {
      const location = this.props.location.pathname + this.props.location.search;
      window.ga('set', 'page', location);
      window.ga('send', 'pageview');
    }
  }

  render() {
    return (
      <div>
        <App
          children={this.props.children}
          setPageTitle={::this.setPageTitle} />
      </div>
    );
  }
}
