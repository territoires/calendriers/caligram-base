import React, { Component } from 'react';
import { stringify } from 'querystring';
import moment from 'moment';
import { index as eventIndex, selectRange } from 'redux/modules/events';
import { setLocale, setTranslations } from 'redux/modules/ui';
import connectData from 'helpers/connectData';

import { EventsMap } from '../components';
import { routes } from '../routes';

function fetchData(getState, dispatch, location) {
  const promises = [];

  const localRoutes = routes.filter((r) => r.id === 'events');
  promises.push(dispatch(setTranslations(localRoutes)));

  const currentRoute = localRoutes.find((r) => r.path === location.pathname);
  promises.push(dispatch(setLocale(currentRoute.locale)));

  if (location.query.range) {
    const range = location.query.range.split(',').map((time) => { return moment(time); });

    if (range.length) {
      const start = range[0] ? range[0].format('YYYY-MM-DD HH:mm:ss') : null;
      const end = range[1] ? range[1].format('YYYY-MM-DD HH:mm:ss') : null;
      promises.push(dispatch(selectRange({start: start, end: end})));
    }
  } else {
    promises.push(dispatch(selectRange({ start: null, end: null })));
  }

  const state = getState();
  const { centerLat, centerLng, radius } = state.events.map;
  const query = {
    distance: `${centerLat},${centerLng},${radius}`,
    order: 'start_date',
    recurrences: false,
    lang: currentRoute.locale,
  };

  promises.push(dispatch(eventIndex(
    location.search === ''
      ? '?' + stringify(query)
      : (location.search + stringify(query))

  )));

  return Promise.all(promises);
}

@connectData(fetchData)
export default class EventsMapContainer extends Component {
  render() {
    return <EventsMap />;
  }
}
