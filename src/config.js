require('babel-polyfill');

const environment = {
  development: {
    isProduction: false,
  },
  production: {
    isProduction: true,
  },
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  app: {
    title: 'Caligram Base',
    description: 'Un calendrier propulsé par Caligram.',
    domain: 'caligram.com',
    meta: {
      charSet: 'utf-8',
      property: {
        'og:site_name': 'Caligram Base',
        'og:image': 'http://api.caligram.com/logo.png',
        'og:locale': 'fr_CA',
        'og:title': 'Caligram base',
        'og:description': 'Un calendrier propulsé par Caligram',
        'twitter:card': 'summary',
        'twitter:site': '@coopterritoires',
        'twitter:creator': '@coopterritoires',
        'twitter:title': 'Caligram',
        'twitter:description': 'Un calendrier propulsé par Caligram.',
        'twitter:image': 'http://api.caligram.com/logo.png',
        'twitter:image:width': '232',
        'twitter:image:height': '58',
      },
    },
    googleAnalytics: {
      id: 'your-google-analytics-id',
    },
    googleMaps: {
      apiKey: 'your-google-maps-api-key',
    },
    googleTagManager: {
      id: null,
    },
    entities: {
      audiences: {
        initialData: null,
        inFilters: true,
        menu: null,
      },
      organizations: {
        inFilters: true,
      },
      regions: {
        inFilters: true,
        limit: 5,
      },
      tags: {
        inFilters: true,
        menuOrder: 'label',
      },
      types: {
        inFilters: true,
        menuOrder: 'label',
        treeView: true,
      },
      venues: {
        inFilters: true,
        limit: 5,
      },
    },
    filtersEntitiesOrder: 'audiences organizations regions tags types venues'.split(' '),
    show: {
      mapToggle: false,
      recurrencesButton: false,
      entityFilters: true,
      disclosableEntities: false,
    },
    showcases: {
      featured: { featured: true },
    },
    map: {
      centerLat: 45.562371,
      centerLng: -73.600579,
      initialZoom: 15,
    },
    externalScripts: [
    ],
    i18n: {
      basePaths: {
        fr: '',
        en: '/en',
      },
    },
  },
}, environment);
