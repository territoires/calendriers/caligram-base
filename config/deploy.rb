APP_NAME = 'base'

require 'mina/multistage'
require 'mina/rails'
require 'mina/git'

set :webhook_url, ' https://hooks.slack.com/services/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'
set :author, "#{`whoami`}".delete!("\n")

set :application_name, "caligram-#{APP_NAME}"
set :user, 'caligram'
set :forward_agent, true

set :shared_dirs, []
set :shared_files, ['.env']

set :keep_releases, 2

task :environment do
end

task :setup => :environment do
end

task :deploy => :environment do

  invoke :'git:ensure_pushed'

  deploy do
    send_message("#{fetch(:author)} déploie #{fetch(:application_name)} (#{fetch(:branch)}) sur #{fetch(:stage)}")
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'

    command 'npm install --production'
    command 'npm run build'

    invoke :'deploy:cleanup'

    on :launch do
      in_path(fetch(:current_path)) do
        if (fetch(:stage).to_sym === :staging)
          command "sudo /usr/sbin/service #{APP_NAME}.staging restart"
        else
          command "sudo /usr/bin/supervisorctl restart #{APP_NAME}"
        end

        command "sudo /usr/sbin/service nginx reload"
        command "sudo /usr/bin/clear_cache #{APP_NAME}"
      end

      send_message("#{fetch(:author)} a déployé #{fetch(:application_name)} (#{fetch(:branch)}) sur #{fetch(:stage)}")
    end

    on :clean do
      send_message("Erreur lors du déploiement de #{fetch(:application_name)} (#{fetch(:branch)}) sur #{fetch(:stage)}")
    end
  end

end

def send_message(message)
  command %[curl -X POST https://#{fetch(:webhook_url)} -d 'payload={"text": "#{message}"}' --silent > /dev/null]
end
